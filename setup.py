from setuptools import setup

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='ortec-ship-route-planner',
      version='2.0',
      description='A ship weather route planner.'
      long_description='A ship route planner for ocean-going vessels optimizing for minimum travel time and/or minimum fuel cost considering environmental conditions.',
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.8',
        'Natural Language :: English',
        'Topic :: Scientific/Engineering :: Mathematics'
      ],
      url='https://gitlab.com/koekoekk/ortec-ship-route-planner',
      author='Job Seuren',
      author_email='job.seuren@gmail.com',
      license='MIT',
      packages='ortec-ship-route-planner',
      install_requires=['dask',
                        'deap',
                        'fiona',
                        'haversine',
                        'more_itertools',
                        'networkx',
                        'numpy',
                        'pandas',
                        'xlrd',
                        'pyproj',
                        'requests',
                        'rtree',
                        'scoop',
                        'scipy',
                        'shapely',
                        'xarray'],
      include_package_data=True,
      zip_safe=False)