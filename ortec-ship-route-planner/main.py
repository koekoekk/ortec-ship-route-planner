import evaluation
import initialization
import math
import numpy as np
import os
import pickle
import random
import support
import uuid

from data_config.navigable_area import NavigableAreaGenerator
from deap import base, creator, tools, algorithms
import geodesic
from operations import Operators
from pathlib import Path
from shapely.geometry import Point

_criteria = {'minimalTime': True, 'minimalCost': True}
timeWeight, costWeight = -5, -1

if _criteria['minimalTime'] and _criteria['minimalCost']:
    _criteria = {'minimalTime': timeWeight, 'minimalCost': costWeight}
elif _criteria['minimalCost']:
    _criteria = {'minimalCost': -1}
else:
    _criteria = {'minimalTime': -1}

creator.create("FitnessMin", base.Fitness, weights=tuple(_criteria.values(),))
creator.create("Individual", list, fitness=creator.FitnessMin)
_tb = base.Toolbox()


def crowding_distance(pop):
    return tools.selNSGA2(pop, len(pop))


class RoutePlanner:
    def __init__(self,
                 constantSpeedIdx=None,  # Index if constant speed to use. If 'None', use variable speed profile
                 vesselName='Fairmaster_2',
                 shipLoading='normal',
                 ecaFactor=1.5593,  # Multiplication factor ECA fuel
                 fuelPrice=300,  # Fuel price per metric tonne
                 bathymetry=False,
                 inputParameters=None,
                 tb=None,
                 criteria=None,
                 seed=None):
        np.random.seed(seed)
        random.seed(seed)

        if criteria is None and constantSpeedIdx is None:
            criteria = _criteria
        else:
            if constantSpeedIdx is None and (criteria['minimalTime'] and criteria['minimalCost']):
                weights = (-1, -1)
            else:
                weights = (-1,)

            creator.FitnessMin.weights = weights

            # creator.create("FitnessMin", base.Fitness, weights=weights)
            # creator.create("Individual", list, fitness=creator.FitnessMin)

        # Set parameters
        defaultParameters = {
                             # Navigation area parameters
                             'avoidAntarctic': True,
                             'avoidArctic': True,
                             'res': 'i',                # Resolution of shorelines
                             'penaltyValue': 1,         # Penalty value for Bathymetry
                             'recursionLvl': 4,         # Main recursion level graph
                             'varRecursionLvl': 6,      # Variable recursion level graph
                             'splits': 3,               # Threshold for split_polygon (3 yields best performance)

                             # MOEA parameters
                             'popSize': 336,            # Population size
                             'nBar': 100,               # Local archive size
                             'crossoverProb': 0.81,     # Crossover probability
                             'mutationProb': 0.28,      # Mutation probability
                             'maxMoves': 9,             # Max. number of mutations per selected individual

                             # Stopping parameters
                             'maxEvaluations': None,
                             'minGenerations': 150,     # Minimal number of generations
                             'maxGDs': 40,              # Max length of generational distance list
                             'minVarianceGD': 1e-6,     # Minimal variance of generational distance list

                             # Mutation parameters
                             'mutationOperators': ['speed', 'insert', 'move', 'delete'],  # Operators to be included
                             'widthRatio': 4.22,        # Shape width ratio for waypoint insertion
                             'radius': 1.35,            # Circle radius for waypoint translation
                             'scaleFactor': 0.1,        # Exponential distribution scale factor for # edges selection
                             'delFactor': 1.2,          # Multiplication factor for 'delete move' selection weight
                             'gauss': False,            # Use Gaussian mutation for insert and move operators

                             # Evaluation parameters
                             'segLengthF': 15,          # Length of linear approx. of great circle track for feasibility
                             'segLengthC': 8            # same for ocean currents and wind along route
                             }

        self.p = {**defaultParameters, **inputParameters} if inputParameters else defaultParameters
        self.tb = _tb if tb is None else tb
        self.criteria = criteria
        self.processedResultsFP = None
        self.vessel = evaluation.Vessel(fuelPrice, vesselName, shipLoading)  # Vessel class instance
        self.fuelPrice = fuelPrice
        self.ecaFactor = ecaFactor
        self.geodesic = geodesic.Geodesic()         # Geodesic class instance
        self.speedIdx = constantSpeedIdx
        self.seed = seed

        # Load and pre-process shoreline, ECA, and Bathymetry geometries
        navAreaGenerator = NavigableAreaGenerator(self.p)
        landTree = navAreaGenerator.get_shoreline_rtree()
        ecaTree = navAreaGenerator.get_eca_rtree()
        bathTree = navAreaGenerator.get_bathymetry_rtree()

        # Initialize "Evaluator" and register it's functions
        self.evaluator = evaluation.Evaluator(self.vessel,
                                              landTree,
                                              ecaTree,
                                              bathTree if bathymetry else None,
                                              ecaFactor,
                                              self.geodesic,
                                              criteria,
                                              self.p)

        # Initialize "Initializer"
        self.initializer = initialization.Initializer(self.evaluator,
                                                      self.vessel,
                                                      landTree,
                                                      ecaTree,
                                                      bathTree,
                                                      self.geodesic,
                                                      self.p,
                                                      creator.Individual,
                                                      constantSpeedIdx,
                                                      seed)

        # Load previously calculated initial routes
        self.initRoutesDir = Path('output/initialRoutes/RES_{}_D{}_VD_{}'.format(self.p['res'], self.p['recursionLvl'],
                                                                                 self.p['varRecursionLvl']))

        self.initRoutesList = []
        if constantSpeedIdx is None:  # Initial paths are loaded only if speedIdx is 'None'
            if os.path.exists(self.initRoutesDir):
                for fp in os.listdir(self.initRoutesDir):
                    with open(self.initRoutesDir / fp, 'rb') as file:
                        self.initRoutesList.append(pickle.load(file))
            else:
                os.mkdir(self.initRoutesDir)

        # Initialize 'Operator' and register it's functions
        self.operators = Operators(self.evaluator.e_feasible, self.vessel, self.geodesic, self.p, seed)
        self.tb.register("mutate", self.operators.mutate)
        self.tb.register("mate", self.operators.cx_one_point)
        self.tb.register("population", initialization.init_repeat_list)

    class Terminator:
        """ Class for MOEA termination.
        """
        def __init__(self, p):
            self.minGenerations = p['minGenerations']
            self.minVarianceGD = p['minVarianceGD']
            self.maxGDs = p['maxGDs']

        def termination(self, prevFront, front, gen, gds):
            # Compute generational distance between current and previous population.
            gd = support.generational_distance(prevFront, front)
            gds.append(gd)

            if len(gds) > self.maxGDs:  # Remove oldest value if gds length is above threshold
                gds.pop(0)
            if gen >= self.minGenerations:  # Do not terminate before minimum nr. of generations
                if np.var(gds) < self.minVarianceGD:  # Terminate if variance is smaller than threshold
                    print('STOP: Variance generational distances exceeded')
                    return True
            return gd

    class NSGA2:
        def __init__(self, tb, evaluator, p, terminator):
            # Set functions / classes
            self.tb = tb
            self.evaluator = evaluator
            self.tb.register("select", tools.selNSGA2)  # Set NSGA2 selection
            self.termination = terminator.termination

            # MOEA parameters
            self.popSize = p['popSize']
            self.crossoverProb = p['crossoverProb']
            self.mutationProb = p['mutationProb']

        def optimize(self, population):
            stats, log = support.statistics(), support.logbook()  # DEAP statistics and logbook
            front = tools.ParetoFront()  # Initialize DEAP ParetoFront
            evaluations = len(population)  # Number of evaluations
            offspring, gds = [], []
            generation = generationalDistance = 0
            while True:
                # Step 3: Environmental selection (and update Pareto front)
                population = self.tb.select(population + offspring, self.popSize)
                prevFront = self.tb.clone(front)
                front.update(population)

                # Record statistics
                record = stats.compile(front)
                log.record(gen=generation, evals=evaluations, gd=generationalDistance, **record)
                print('\r', end='')
                print(log.stream)

                # Step 4: Termination
                generationalDistance = self.termination(prevFront, front, generation, gds)
                if not isinstance(generationalDistance, float):
                    return front, log

                # Step 5: Variation
                offspring = algorithms.varAnd(population, self.tb, self.crossoverProb, self.mutationProb)

                # Step 2: Fitness assignment
                invalidIndividuals = [ind for ind in offspring if not ind.fitness.valid]
                fits = self.tb.map(self.evaluator.evaluate, invalidIndividuals)
                for ind, fit in zip(invalidIndividuals, fits):
                    ind.fitness.values = fit

                evaluations = len(invalidIndividuals)

                generation += 1

    def compute(self, startEnd, recompute=False, startDate=None, current=False,
                weather=False, avoidArctic=True, avoidAntarctic=True):
        support.clear_caches()  # Clear caches
        start, end = startEnd
        if start == end:
            return 'equal_start_end'

        # Check whether start and/or end point lies within the (ant)arctic circle
        for point in startEnd:
            if avoidAntarctic and support.antarctic_circle.contains(Point(point)):
                avoidAntarctic = False
            if avoidArctic and support.arctic_circle.contains(Point(point)):
                avoidArctic = False

        # Check whether output is previously computed
        dateString = None if startDate is None else startDate.strftime('%Y%m%d')
        outputFP = "{}_C{}_W{}_d{}_inclS{}_inclN{}_V{}_T{}_C{}_FP{}_ECA{}".format(startEnd, current, weather,
                                                                            dateString, avoidAntarctic,
                                                                            avoidArctic, self.vessel.name,
                                                                            True,
                                                                            True,
                                                                            self.fuelPrice, self.ecaFactor)
        self.processedResultsFP = Path('output/processed/') / outputFP

        if not recompute and os.path.exists(self.processedResultsFP):
            print('Routes previously recomputed, no recomputing needed')
            return 'precomputed'

        # Update parameters if (ant)arctic circle avoidance is changed
        newParameters = {}
        if self.p['avoidAntarctic'] != avoidAntarctic or self.p['avoidArctic'] != avoidArctic:
            print('Updating parameter as (ant)arctic circle avoidance is changed')
            newParameters['avoidAntarctic'] = avoidAntarctic
            newParameters['avoidArctic'] = avoidArctic
        self.update_parameters(newParameters)

        # Load initial routes
        initRoutes = None
        for initRouteDict in self.initRoutesList:
            if initRouteDict['startEnd'] == startEnd and initRouteDict['avoidAntarctic'] == avoidAntarctic and \
                    initRouteDict['avoidArctic'] == avoidArctic:
                initRoutes = initRouteDict['paths']
                print('Loaded previously computed initial routes')
                break

        # If no previously calculated initial routes loaded, calculate and save initial routes
        if initRoutes is None:
            print('Start calculation initial routes')
            initRoutes = self.initializer.get_initial_routes(start, end)
            initRoutesOutput = {'avoidAntarctic': avoidAntarctic,
                                'avoidArctic': avoidArctic,
                                'startEnd': startEnd,
                                'paths': initRoutes}
            self.initRoutesList.append(initRoutesOutput)
            initRoutesFN = str(uuid.uuid4())
            with open(self.initRoutesDir / initRoutesFN, 'wb') as file:
                pickle.dump(initRoutesOutput, file)

        terminator = self.Terminator(self.p)  # Initialize 'terminator' class

        # Assign NSGA2 class (SPEA2 and M-PAES algorithms have been removed for simplicity)
        MOEA = self.NSGA2(self.tb, self.evaluator, self.p, terminator)

        # Initialize output dictionary
        output = {'startEnd': startEnd,
                  'initialRoutes': initRoutes,
                  'logs': [None] * len(initRoutes),
                  'fronts': [None] * len(initRoutes)}

        # Optimize each (sub-)initial route
        for routeIdx, route in enumerate(initRoutes):
            # Initialize output dictionary
            output['logs'][routeIdx] = [None] * len(route['route'])
            output['fronts'][routeIdx] = [None] * len(route['route'])

            print('Computing route {}/{}'.format(routeIdx + 1, len(initRoutes)))
            for subRouteIdx, subRoute in enumerate(route['route']):
                print('Computing sub route {}/{}'.format(subRouteIdx + 1, len(route['route'])))

                # Register individual function
                self.tb.register("individual", initialization.init_individual, self.tb, subRoute)

                # Initialize population
                print('Initializing population from shortest path:', end='\n ')
                initialPop = self.tb.population(self.tb.individual, int(MOEA.popSize / len(subRoute.values())))
                print('done')

                # Assign fitness to initial population
                print('Fitness assignment:', end='')
                fits = self.tb.map(self.evaluator.evaluate, initialPop)
                for ind, fit in zip(initialPop, fits):
                    ind.fitness.values = fit
                print('\rFitness assigned')
                self.evaluator.set_classes(current, weather, startDate, self.get_days(initialPop))
                self.tb.register("individual", initialization.init_individual, self.tb, subRoute)

                # Start generational process
                front, log = MOEA.optimize(initialPop)
                self.tb.unregister("individual")

                # Save 'raw' results
                output['logs'][routeIdx][subRouteIdx] = self.tb.clone(log)
                output['fronts'][routeIdx][subRouteIdx] = self.tb.clone(front)

        return output

    def update_parameters(self, newParameters):
        self.p = {**self.p, **newParameters}
        self.operators = Operators(self.evaluator.e_feasible, self.vessel, self.geodesic, self.p, self.seed)

        if 'avoidAntarctic' in newParameters or 'avoidArctic' in newParameters:
            # Re-populate R-Tree structures
            navAreaGenerator = NavigableAreaGenerator(self.p)
            self.evaluator.landRtree = navAreaGenerator.get_shoreline_rtree()
            self.evaluator.ecaRtree = navAreaGenerator.get_eca_rtree()
            self.initializer = initialization.Initializer(self.evaluator, self.vessel, self.evaluator.landRtree,
                                                          self.evaluator.ecaRtree, self.initializer.bathTree,
                                                          self.geodesic, self.p, creator.Individual, self.speedIdx,
                                                          self.seed)

    def get_days(self, pop):
        """
        Get estimate of max travel time of individuals in *pop* in whole days.
        Minimal returned days is 30.
        """
        boatSpeed = min(self.vessel.speeds)
        maxTravelTime = 0.
        for ind in pop:
            travelTime = 0.
            for i in range(len(ind) - 1):
                p1, p2 = (ind[i][0], ind[i+1][0])
                edgeDist = self.geodesic.distance(p1, p2)
                edgeTravelTime = edgeDist / boatSpeed
                travelTime += edgeTravelTime
            if travelTime > maxTravelTime:
                maxTravelTime = travelTime
        days = int(math.ceil(maxTravelTime / 24))
        print('Conservative estimate number of days:', days)
        return min(days, 30)

    def create_route_response(self, obj, bestWeighted, wps, objValue, fitValue, xCanals):
        return {'optimizationCriterion': obj,
                'bestWeighted': bestWeighted,
                'distance': self.geodesic.total_distance(wps),
                'fuelCost': objValue[1],
                'travelTime': objValue[0],
                'fitValues': fitValue.tolist(),
                'waypoints': [{'lon': wp[0][0],
                               'lat': wp[0][1],
                               'speed': wp[1]} for wp in wps],
                'crossedCanals': xCanals}

    def post_process(self, rawOutput, ID=None):
        if rawOutput == 'precomputed':
            with open(self.processedResultsFP, 'rb') as file:
                processedOutput = pickle.load(file)
            with open(self.processedResultsFP.as_posix() + '_raw', 'rb') as file:
                rawOutput = pickle.load(file)
            return processedOutput, rawOutput
        elif rawOutput == 'equal_start_end':
            processedOutput = {'routeResponse': [],
                               'units': {'travelTime': 'days',
                                         'fuelCost': 'USD, x1000',
                                         'distance': 'nautical miles'}}

            for obj in [obj for obj, included in self.criteria.items() if included]:
                processedOutput['routeResponse'].append({'optimizationCriterion': obj,
                                                         'bestWeighted': False,
                                                         'distance': 0.,
                                                         'fuelCost': 0.,
                                                         'travelTime': 0.,
                                                         'fitValues': [0., 0.],
                                                         'waypoints': [],
                                                         'crossedCanals': []})
            return processedOutput

        nFits = len([included for included in self.criteria.values() if included])
        objKeys = [obj for obj, included in self.criteria.items() if included]
        objIndices = {'minimalTime': 0, 'minimalCost': 1}
        processedOutput = {'routeResponse': [],
                           'initialRoutes': rawOutput['initialRoutes'],
                           'units': {'travelTime': 'days',
                                     'fuelCost': 'USD, x1000',
                                     'distance': 'nautical miles'}}
        if ID:
            processedOutput['id'] = ID

        # Get minimum fuel route and minimum time route for each path
        # Then create output dictionary
        for i, pathFront in enumerate(rawOutput['fronts']):
            bestWeighted = {'bestWeighted': True}
            xCanals = rawOutput['initialRoutes'][i]['xCanals']

            # Get bestWeighted route
            wps, bestWeightedObjValue, fitValue = [], np.zeros(2), np.zeros(nFits)
            for subFront in pathFront:
                ind = subFront[0]
                bestWeightedObjValue += self.evaluator.evaluate(ind, revert=False, includePenalty=False)
                fitValue += ind.fitness.values
                wps.extend(ind)

            bestWeightedResponse = self.create_route_response('bestWeighted',
                                                              True,
                                                              wps,
                                                              bestWeightedObjValue,
                                                              fitValue,
                                                              xCanals)

            for obj in objKeys:
                i = objIndices[obj]

                # Initialize lists and arrays
                wps, objValue, fitValue = [], np.zeros(2), np.zeros(nFits)

                # A path is split into sub paths by canals, so we merge the sub path results
                for subFront in pathFront:
                    # Evaluate fuel and time for each individual in front
                    # NB: Using same parameter settings for Evaluator as in optimization
                    subObjValues = np.asarray([self.evaluator.evaluate(ind, revert=False, includePenalty=False)
                                               for ind in subFront])

                    # Get best individual
                    idx = np.argmin(subObjValues[:, i])
                    ind = subFront[idx]
                    wps.extend(ind)
                    objValue += subObjValues[idx]
                    fitValue += ind.fitness.values

                # Check whether 'best weighted' route is equal to other best routes
                if np.array_equal(objValue, bestWeightedObjValue):
                    print("'{}' is best weighted route".format(obj))
                    bestWeighted[obj] = True
                    bestWeighted['bestWeighted'] = False
                else:
                    bestWeighted[obj] = False

                routeResponse = self.create_route_response(obj=obj,
                                                           bestWeighted=bestWeighted[obj],
                                                           wps=wps,
                                                           fitValue=fitValue,
                                                           objValue=objValue,
                                                           xCanals=xCanals)

                processedOutput['routeResponse'].append(routeResponse)

            # If 'best weighted' route is not equal to other best routes, append its response
            if bestWeighted['bestWeighted']:
                processedOutput['routeResponse'].append(bestWeightedResponse)

        try:
            with open(self.processedResultsFP, 'wb') as file:
                pickle.dump(processedOutput, file)
            with open(self.processedResultsFP.as_posix() + '_raw', 'wb') as file:
                pickle.dump(rawOutput, file)
        except TypeError:
            print("Save filepath is 'None': Result is not saved")

        return processedOutput, rawOutput


if __name__ == "__main__":
    import pprint
    import time

    from datetime import datetime
    from scoop import futures
    from support import locations

    execution_start_time = time.time()
    start_end = (locations['Floro'], locations['Santander'])
    start_date = datetime(2017, 9, 4)
    kwargsPlanner = dict(inputParameters={}, tb=_tb, ecaFactor=1.0, constantSpeedIdx=None, vesselName='Fairmaster',
                         criteria=_criteria)
    kwargsCompute = dict(startEnd=start_end, startDate=start_date, recompute=True, current=False, weather=False)
    multiprocess = False

    if multiprocess:  # Multiprocessing does not work appropriately
        _tb.register("map", futures.map)

        planner = RoutePlanner(**kwargsPlanner)
        raw_output = planner.compute(**kwargsCompute)
    else:
        planner = RoutePlanner(**kwargsPlanner)
        raw_output = planner.compute(**kwargsCompute)

    print("--- %s seconds ---" % (time.time() - execution_start_time))
    processed_output, raw_output = planner.post_process(raw_output)
    pp = pprint.PrettyPrinter(depth=6)
    pp.pprint(processed_output)
