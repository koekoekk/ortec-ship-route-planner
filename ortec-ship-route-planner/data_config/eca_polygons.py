import fiona
import numpy as np
import pandas as pd
import pickle

from mpl_toolkits.basemap import Basemap
from shapely.geometry import Polygon, shape


def create_from_csv(fp, save=True):
    df = pd.read_csv(fp)
    area_names = df.area.unique()
    ecas = []
    for area_name in area_names:
        area_df = df.loc[df['area'] == area_name]
        points = [[row['longitude'], row['latitude']] for idx, row in area_df.iterrows()]
        ecas.append(Polygon([pt for pt in points]))
    if save:
        fp = 'data/navigation_area/ecas/ecas_csv'
        with open(fp, 'wb') as file:
            pickle.dump(ecas, file)
        print('Saved ecas to', fp)

    return ecas


def create_from_shp(fp, save=True):
    ecas = []
    shapely_objects = [shape(poly_shape['geometry']) for poly_shape in iter(fiona.open(fp.as_posix()))]
    for shapely_object in shapely_objects:
        if shapely_object.geom_type == 'MultiPolygon':
            ecas.extend(list(shapely_object))
        elif shapely_object.geom_type == 'Polygon':
            ecas.append(shapely_object)
        else:
            raise IOError('Shape is not a polygon.')
    if save:
        fp = 'data/navigation_area/ecas/ecas_shp'
        with open(fp, 'wb') as file:
            pickle.dump(ecas, file)
        print('Saved secas to', fp)

    return ecas


def plot_from_list(ecas):
    m = Basemap(projection='merc', resolution='l', llcrnrlat=-80, urcrnrlat=80, llcrnrlon=-180, urcrnrlon=180)
    m.drawparallels(np.arange(-90., 90., 10.), labels=[1, 0, 0, 0], fontsize=10)
    m.drawmeridians(np.arange(-180., 180., 10.), labels=[0, 0, 0, 1], fontsize=10)
    m.drawcoastlines()
    m.fillcontinents()

    for poly in ecas:
        lon, lat = poly.exterior.xy
        x, y = m(lon, lat)
        m.plot(x, y, 'o-', markersize=2, linewidth=1)


if __name__ == '__main__':
    csv_fp = 'data/navigation_area/ecas/ecas_csv.csv'
    shp_fp = 'data/navigation_area/eca_reg14_sox_pm/eca_reg14_sox_pm.shp'

    # shp_ecas = create_from_shp(shp_fp, save=True)
    # csv_ecas = create_from_csv(csv_fp, save=False)
