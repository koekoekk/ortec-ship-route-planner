import datetime
import numpy as np
import os
import xarray as xr

from ftplib import FTP
from pathlib import Path
from os import path


class CurrentDataRetriever:
    def __init__(self,
                 t0,
                 nDays,
                 host='eftp.ifremer.fr',
                 user='gg1f3e8',
                 pwd='xG3jZhT9'
                 ):
        # Current data period
        self.nDays = nDays
        self.tStart = t0

        # FTP login details
        self.host = host
        self.user = user
        self.pwd = pwd

        # Get dataset file path and download and save directories
        d = Path('data/currents/netcdf_OUT/')
        f = 'Y%d_YDAY%03d_NDAYS%03d.nc' % (t0.year, t0.timetuple().tm_yday, nDays)
        self.dataFP = Path(d / f)
        self.dsFTP = Path('/data/globcurrent/v3.0/global_025_deg/total_hs')
        self.dataDir = Path('data/currents/')
        self.dsSave = self.dataDir / 'netcdf_IN'

    def get_data(self):
        # First check if combined netCDF file exists
        if os.path.exists(self.dataFP):
            print('Loading current data into memory:'.format(self.dataFP), end=' ')
            with xr.open_dataset(self.dataFP) as ds:
                print('done')
                return ds.to_array().data
        else:  # Create combined netCDF file from separate (to-be) downloaded netCDF files
            # Download non-existent netCDF files
            saveFPs = []
            with FTP(host=self.host, user=self.user, passwd=self.pwd) as ftp:
                for day in range(self.nDays):
                    # Get path appendix
                    t = self.tStart + datetime.timedelta(days=day)
                    y, yday = t.year, t.timetuple().tm_yday
                    path_appendix = Path('%d/%03d' % (y, yday))

                    # Set FTP current working directory and save directory
                    ftp.cwd(Path(self.dsFTP / path_appendix).as_posix())
                    saveDir = Path(self.dsSave / path_appendix)
                    if not path.exists(saveDir):
                        os.makedirs(saveDir)

                    # Append files to file_list
                    files = [f for f in ftp.nlst() if '0000' in f]
                    _saveFPs = [Path(saveDir / f).as_posix() for f in files]
                    saveFPs.extend(_saveFPs)
                    for saveFP, loadFP in zip(_saveFPs, files):
                        if not path.isfile(saveFP):  # If files does not exist, download from FTP server
                            with open(saveFP, 'wb') as f:  # Download file to fp_save
                                ftp.retrbinary('RETR %s' % loadFP, f.write)

            # Open downloaded netCDF files, combine and store locally
            print('Combining %d netCDF files:' % (8 * self.nDays), end=' ')
            with xr.open_mfdataset(saveFPs, parallel=True, combine='by_coords', preprocess=ms_to_knots) as ds:
                print("done\nStoring data array to '{}' :".format(self.dataFP), end=' ')
                ds.to_netcdf(self.dataFP)
                print('done')
                return ds.to_array().data


def ms_to_knots(ds):
    ds.attrs = {}
    arr2d = np.float32(np.ones((720, 1440)) * 1.94384)
    ds['u_knot'] = arr2d * ds['eastward_eulerian_current_velocity']
    ds['v_knot'] = arr2d * ds['northward_eulerian_current_velocity']
    ds = ds.drop_vars(['eastward_eulerian_current_velocity',
                       'eastward_eulerian_current_velocity_error',
                       'northward_eulerian_current_velocity',
                       'northward_eulerian_current_velocity_error'])
    return ds


if __name__ == '__main__':
    import matplotlib.pyplot as plt

    from mpl_toolkits.basemap import Basemap
    from pathlib import Path


    def plot_currents(uin, vin, lons, lats, extent):
        m = Basemap(projection='merc', resolution='l',
                    llcrnrlon=extent[0], llcrnrlat=extent[1], urcrnrlon=extent[2], urcrnrlat=extent[3])

        # Number of quivers in vertical (lat) and horizontal (lon) direction
        vLat = int((max(lats) - min(lats)) * 6)
        vLon = int((max(lons) - min(lons)) * 6)

        # Transform data points
        uRot, vRot, x, y = m.transform_vector(uin, vin, lons, lats, vLon, vLat, returnxy=True)

        # Plot contour
        C = m.contourf(x, y, np.hypot(uRot, vRot), cmap='Blues')
        cb = m.colorbar(C, size=0.2, pad=0.05, location='right')
        cb.set_label('Current velocity [kn]', rotation=270, labelpad=15)

        # Plot vectors (quiver)
        Q = m.quiver(x, y, uRot, vRot, np.hypot(uRot, vRot), cmap='Greys', pivot='mid', width=0.002, headlength=4,
                     scale=90)
        plt.quiverkey(Q, 0.45, -0.1, 2, r'$2$ knots', labelpos='E')

        # Add coastlines and parallels/meridians
        m.drawcoastlines()
        m.fillcontinents()
        m.drawmapboundary()
        plt.title('Ocean currents')
        m.drawparallels(np.arange(-90., 90., 30.), labels=[1, 0, 0, 0])
        m.drawmeridians(np.arange(-180., 180., 30.), labels=[0, 0, 0, 1])
