import datetime
import numpy as np
import os
import pandas as pd
import support
import environmental_conditions

from functools import lru_cache, wraps
from math import cos, sin, sqrt, radians, pow
from pathlib import Path
from shapely.geometry import LineString, Point


def delta_penalty(func):
    @wraps(func)
    def wrapper(self, individual, *args, **kwargs):
        return func(self, individual, *args, **kwargs) if self.feasible(individual) else self.delta
    return wrapper


class Evaluator:
    def __init__(self,
                 vessel,
                 landRtree,
                 ecaRtree,
                 bathRtree,
                 ecaFactor,
                 geodesic,
                 criteria,
                 parameters,
                 startDate=None):
        self.vessel = vessel            # Vessel class instance
        self.landRtree = landRtree      # R-tree spatial index dictionary for shorelines
        self.ecaRtree = ecaRtree        # R-tree spatial index dictionary for ECAs
        self.bathRtree = bathRtree      # R-tree spatial index dictionary for bathymetry
        self.ecaFactor = float(ecaFactor)  # Multiplication factor for ECA fuel
        self.startDate = startDate      # Start date of voyage
        self.segLengthF = parameters['segLengthF']    # Max segment length (for feasibility)
        self.segLengthC = parameters['segLengthC']    # Max segment length (for currents)
        self.geodesic = geodesic        # Geodesic class instance
        self.currentOp = None
        self.weatherOp = None
        self.weather = None
        self.current = None
        self.bathymetry = False if bathRtree is None else True
        self.criteria = criteria
        self.includePenalty = False
        self.revertOutput = not criteria['minimalTime']
        self.penaltyValue = parameters['penaltyValue']
        self.delta = (1e+20,) * len(criteria)

    def set_classes(self, current, weather, startDate, nDays):
        self.startDate = startDate
        self.weather = weather
        self.current = current
        forecast = True if weather and weather != 'historical' else False
        if current:
            assert isinstance(startDate, datetime.date), 'Set start date'
            self.currentOp = environmental_conditions.CurrentOperator(startDate, nDays)
        if weather:
            assert isinstance(startDate, datetime.date), 'Set start date'
            self.weatherOp = environmental_conditions.WindOperator(startDate, nDays, forecast)

    @delta_penalty
    def evaluate(self, ind, revert=None, includePenalty=None):
        self.includePenalty = self.bathymetry if includePenalty is None else includePenalty
        if revert is None:
            revert = self.revertOutput

        hours = cost = dist = ecaDist = 0.

        for wp1, wp2 in zip(ind[:-1], ind[1:]):
            # Leg endpoints and boat speed
            p1, speedKnots = wp1
            p2 = wp2[0]

            # Leg travel time and fuel cost
            legHours, nauticalMiles = self.leg_hours(p1, p2, hours, speedKnots)
            legCost = self.vessel.fuelCostPerDay[speedKnots] * legHours / 24.  # x1000 EUR or USD

            # If leg intersects ECA increase fuel consumption by ecaFactor
            if self.ecaFactor != 1.0 and geo_x_geos(self.ecaRtree, p1, p2):
                legCost *= self.ecaFactor
                ecaDist += nauticalMiles

            if self.includePenalty:
                nSegments = self.e_feasible(p1, p2)
                legCost += (nSegments * self.segLengthF / nauticalMiles) * legCost * self.penaltyValue
                legHours += (nSegments * self.segLengthF / nauticalMiles) * legHours / 24. * self.penaltyValue

            # Increment objective values
            hours += legHours
            cost += legCost
            dist += nauticalMiles

        days = hours / 24.
        if revert:
            return cost, days
        return days, cost

    def feasible(self, ind):
        for i in range(len(ind)-1):
            p1, p2 = sorted((ind[i][0], ind[i+1][0]))
            if self.e_feasible(p1, p2) == 'inf':
                return False
        return True

    @lru_cache(maxsize=None)
    def e_feasible(self, p1, p2):
        dist = self.geodesic.distance(p1, p2)
        lons, lats = self.geodesic.points(p1, p2, dist, self.segLengthF)
        lineSegments = np.stack([lons, lats]).T

        # Check for datum line wrap arounds (e.g., lon0 -> lon1 = 179 degrees -> -179 degrees)
        # since we know the difference between any two points, we can use this to find wrap arounds
        dLonMax = self.segLengthF * 10. / 60.

        # calculate distances and compare with max allowable distance
        dLons = np.abs(np.diff(lons))
        cuts = np.where(dLons > dLonMax)[0]

        # if there are any cut points, cut them and begin again at the next point
        for i, cut in enumerate(cuts):
            # create new vertices with a nan in between and set those as the path's vertices
            segments = np.concatenate([lineSegments[:cut+1, :],
                                      [[np.nan, np.nan]],
                                      lineSegments[cut+1:, :]]
                                      )
            lineSegments = segments

        # Check feasibility of each segment, and penalize sailing through shallow water
        nSegments = 0
        for q1, q2 in zip(lineSegments[:-1, :], lineSegments[1:, :]):
            if not np.isnan(np.sum([q1, q2])):  # Skip segments crossing datum line
                if geo_x_geos(self.landRtree, q1, q2):  # If segment crosses land obstacle
                    return 'inf'
                if self.includePenalty and not geo_x_geos(self.bathRtree, q1, q2):
                    nSegments += 1
        return nSegments

    @lru_cache(maxsize=None)
    def leg_hours(self, p1, p2, startHours, speedKnots):
        nauticalMiles = self.geodesic.distance(p1, p2)
        if self.current or self.weather:
            # Split leg in equally spaces segments of 'segLengthC' nautical miles
            lons, lats = self.geodesic.points(p1, p2, nauticalMiles, self.segLengthC)
            segPoints = list(zip(lons, lats))
            legHours = 0.
            for q1, q2 in zip(segPoints[:-1], segPoints[1:]):
                legHours += self.calc_seg_hours(q1, q2, speedKnots, startHours + legHours)
        else:
            legHours = nauticalMiles / speedKnots
        return legHours, nauticalMiles

    def calc_seg_hours(self, p1, p2, speedKnots, currentHours):
        try:
            now = self.startDate + datetime.timedelta(hours=currentHours)
        except TypeError:
            raise TypeError("Date {} ; currHours {}".format(self.startDate, currentHours))
        nauticalMiles, bearingDeg = self.geodesic.distance(p1, p2, bearing=True)

        # Coordinates of middle point of leg
        # If segment crosses datum line (-180 degrees), choose p1 as middle point
        lon, lat = p1 if abs(p1[0] - p2[0]) > 300 else ((p1[0] + p2[0]) / 2, (p1[1] + p2[1]) / 2)
        if self.weather:
            # Beaufort number (BN) and true wind direction (TWD) at (lon, lat)
            BN, windDeg = self.weatherOp.get_grid_pt_wind(now, lon, lat)
            headingDeg = bearingDeg
            speedKnots = self.vessel.reduced_speed(windDeg, headingDeg, BN, speedKnots)

        if self.current:
            # Eastward (Se) and northward (Sn) current velocities at (lon, lat) in knots
            Se, Sn = self.currentOp.get_grid_pt_current(now, lon, lat)

            # Calculate speed over ground (actualSpeed)
            actualSpeedKnots = calc_sog(radians(bearingDeg), Se, Sn, speedKnots)
        else:
            actualSpeedKnots = speedKnots

        return nauticalMiles / actualSpeedKnots


class Vessel:
    def __init__(self, fuelPrice, name='Fairmaster_2', shipLoading='normal', speeds=None):
        self.name = name
        vesselTableFP = 'data/speed_table.xlsx'

        if os.path.exists(vesselTableFP):
            df = pd.read_excel(vesselTableFP, sheet_name=self.name)
            df = df[df['Loading'] == shipLoading].round(1)
            self.fuelCostPerDay = pd.Series(df.Fuel.values * fuelPrice / 1000., index=df.Speed).to_dict()
            if speeds is None:
                self.speeds = list(self.fuelCostPerDay.keys())
            elif isinstance(speeds, float) or isinstance(speeds, int):
                self.speeds = [speeds]
            elif isinstance(speeds, list):
                self.speeds = speeds
            else:
                raise ValueError('Provide speed profile as list, float or integer')

        else:  # Use empirical formula for fuel consumption
            print('Approximate fuel consumption with nonlinear function')

            def fc(v):
                """Approximate fuel consumption per day nonlinear function from (Psaraftis and Kontovas, 2013)
                    Arguments:
                        v: Vessel speed in knots
                    Returns:
                        Fuel consumption in tonnes per day
                  """
                return 5.466e-4 * v**3 * 24

            if isinstance(speeds, float) or isinstance(speeds, int):
                self.speeds = [speeds]
            elif isinstance(speeds, list):
                self.speeds = speeds
            else:
                raise ValueError('Vessel speed profile not provided', '\n', speeds)
            self.fuelCostPerDay = {speed: fc(speed) for speed in self.speeds}

        # Set parameters for ship speed reduction calculations  
        Lpp = 152.9  # Ship length between perpendiculars [m]
        vol = 27150  # Displaced volume [m^3]
        blockCoefficient = 0.8
        self.speed_reduction = SemiEmpiricalSpeedReduction(blockCoefficient, shipLoading, Lpp, vol)
        self.reduced_speed = self.speed_reduction.reduced_speed


class SemiEmpiricalSpeedReduction:
    """Based on Kwon's method for calculating the reduction of ship speed as a function of wind direction and speed.
    Kwon, Y.J., 2008. Speed loss due to added resistance in wind and waves """

    def __init__(self, block, shipLoading, Lpp, volume):

        coefficientTableFP = 'data/kwons_method_coefficient_tables.xlsx'
        # Weather direction reduction table
        df = pd.read_excel(coefficientTableFP, sheet_name='direction_reduction_coefficient')
        self.directionDF = df[['a', 'b', 'c']].to_numpy()
        # self.windAngleBins = [30, 60, 150, 180]

        # Speed reduction formula coefficients: a, b, c
        if shipLoading == 'ballast':
            blockBins = np.asarray([0.75, 0.8, 0.85])
        else:
            blockBins = np.asarray([0.6, 0.65, 0.7, 0.75, 0.8, 0.85])

        roundedBlock = blockBins[support.find_closest(blockBins, block)]
        df = pd.read_excel(coefficientTableFP, sheet_name='speed_reduction_coefficient')
        abc = df.loc[(df['block_coefficient'] == roundedBlock) & (df['ship_loading'] == shipLoading)]
        self.aB, self.bB, cB = float(abc['a']), float(abc['b']), float(abc['c'])
        g = 9.81  # gravitational acceleration [m/s^2]
        knotToMs = 0.514444
        self.FnConstant = knotToMs / sqrt(Lpp * g)
        self.cB = cB * self.FnConstant ** 2

        # Ship coefficient formula coefficients: a, b
        df = pd.read_excel(coefficientTableFP, sheet_name='ship_form_coefficient')
        ab = df.loc[(df['ship_type'] == 'all') & (df['ship_loading'] == shipLoading)]
        self.aU, bU = float(ab['a']), float(ab['b'])

        self.formDenominator = 1 / (bU * pow(volume, (2 / 3)))

    def reduced_speed(self, windDeg, headingDeg, BN, speedKnots):
        """ The wind effect, presented as speed loss, compares the
        speed of the ship in varying actual sea conditions to the ship's
        expected speed in still water conditions.
        formC is the
        directionC is the ,
        speedC is is the  """

        # General speed loss in head wind condition
        formC = self.aU * BN + pow(BN, 6.5) * self.formDenominator

        # Weather direction reduction factor
        windAngle = abs((windDeg - headingDeg + 180) % 360 - 180)  # [0, 180] degrees
        if windAngle < 30:
            abc = self.directionDF[0]
        elif windAngle < 60:
            abc = self.directionDF[1]
        elif windAngle < 150:
            abc = self.directionDF[2]
        else:
            abc = self.directionDF[3]
        a, b, c = abc[0], abc[1], abc[2]
        directionC = (a + b * pow(BN + c, 2)) / 2

        # Correction factor for block coefficient and Froude number
        Fn = speedKnots * self.FnConstant
        speedC = self.aB + self.bB * Fn + self.cB * pow(speedKnots, 2)
        speedLoss = max(min((directionC * speedC * formC) / 100, 0.99), -0.3)

        reducedSpeedKnots = speedKnots * (1 - speedLoss)
        return reducedSpeedKnots


def calc_sog(bearingRad, Se, Sn, V):
    """
    Determine speed over ground (sog) between points p1 and p2 in knots.
    see thesis section Ship Speed
    """

    # Calculate speed over ground (sog)
    sinB, cosB = sin(bearingRad), cos(bearingRad)
    try:
        result = Se * sinB + Sn * cosB + sqrt(V * V - (Se * cosB - Sn * sinB) ** 2)
    except ValueError:
        print('ValueError')
        result = V
    return result


def geo_x_geos(treeDict, p1, p2=None):
    p2 = p1 if p2 is None else p2
    minx, maxx = (p1[0], p2[0]) if p1[0] < p2[0] else (p2[0], p1[0])
    miny, maxy = (p1[1], p2[1]) if p1[1] < p2[1] else (p2[1], p1[1])
    bounds = (minx, miny, maxx, maxy)

    # Return the geometry indices whose bounds intersect the query bounds
    indices = treeDict['rtree'].intersection(bounds)
    if indices:
        shapelyObject = Point(tuple(p1)) if p2 is None else LineString([tuple(p1), tuple(p2)])
        for idx in indices:
            geometry = treeDict['geometries'][idx]
            if p2 is None and geometry.contains(shapelyObject):
                return True
            elif geometry.intersects(shapelyObject):
                return True
    return False
