import numpy as np
import os
import pandas as pd
import pickle
import pprint
import time

from datetime import datetime
from main import RoutePlanner

# Get start and end locations
locations = pd.read_excel('input_locations.xlsx')
os.chdir('../..')
print(os.getcwd())
startLocations, endLocations = {}, {}
for idx, row in locations.iterrows():
    if row['from'] == 1:
        startLocations[row['name']] = (row['lon'], row['lat'])
    if row['to'] == 1:
        endLocations[row['name']] = (row['lon'], row['lat'])

count = 0

# Initialize route planner
planner = RoutePlanner(criteria={'minimalTime': True, 'minimalCost': False})

# For each start/end combination, calculate route and store results
resultsDict, times = {}, []
for startKey, start in startLocations.items():
    for endKey, end in endLocations.items():
        if startKey != 'A':
            continue
        count += 1
        print('ROUTE: {}/{}'.format(count, len(startLocations) + len(endLocations)))
        print('\n\n', startKey, start)
        print(endKey, end, '\n\n')

        t0 = time.time()
        result = planner.compute(tuple(sorted((start, end))), recompute=True)
        t1 = time.time()
        if result != 'equal_start_end':
            times.append(t1 - t0)
            print('INTERIM_AVERAGE', np.average(times))
        resultsDict[(startKey, endKey)] = planner.post_process(result)

times = np.array(times)
print(times)
print('avg', np.average(times), 'std', np.std(times), 'len', len(times))


# Print results
pp = pprint.PrettyPrinter(depth=5)
pp.pprint(resultsDict)

# Save results
resultFN = '{0:%H_%M_%S}'.format(datetime.now())
fp = 'applications/project_locations/' + resultFN
with open(fp, 'wb') as file:
    pickle.dump(resultsDict, file)
print('Saved result to: ', fp)
