import math
import matplotlib.pyplot as plt
import pandas as pd
import pickle
import pprint

from deap import base, creator
from mpl_toolkits.basemap import Basemap

fp = 'xxx'  # set to output file of 'distances_between_projects.py'
with open(fp, 'rb') as file:
    inputResults = pickle.load(file)


# Create Fitness and Individual types
creator.create("FitnessMin", base.Fitness, weights=(-1,))
creator.create("Individual", list, fitness=creator.FitnessMin)


specificLocation = ('Bahia', 'Mumbai')
inclCanals = 'all'  # choose 'none', 'panama', 'suez', or 'both' for Panama and Suez


def plot_routes(waypoints, ax, title):
    ax.title.set_text(title)
    m = Basemap(projection='merc', resolution='c', llcrnrlat=-70, urcrnrlat=75, llcrnrlon=-180, urcrnrlon=180, ax=ax)
    m.drawcoastlines()
    m.fillcontinents()

    lons, lats = [wp['lon'] for wp in waypoints], [wp['lat'] for wp in waypoints]

    # Plot edges and points
    for wpIdx in range(len(waypoints)):
        lon1, lat1 = lons[wpIdx], lats[wpIdx]
        if wpIdx < len(waypoints) - 1:
            lon2, lat2 = lons[wpIdx+1], lats[wpIdx+1]
            m.drawgreatcircle(lon1, lat1, lon2, lat2, linewidth=1, color='red', zorder=1)
        m.scatter(lon1, lat1, latlon=True, color='dimgray', marker='o', s=1, zorder=2)


# Print input
pp = pprint.PrettyPrinter(depth=4)
# pp.pprint(inputResults)

dists = {'both': {}, 'suez': {}, 'panama': {}, 'none': {}}
wps = {'both': {}, 'suez': {}, 'panama': {}, 'none': {}}
wpsSpecificLocation = {'both': {}, 'suez': {}, 'panama': {}, 'none': {}}


for startEnd, resultValue in inputResults.items():
    for response in resultValue['routeResponse']:
        if 'Suez' in response['crossedCanals'] and 'Panama' in response['crossedCanals']:
            dists['both'][startEnd] = response['distance']
            wps['both'][startEnd] = response['waypoints']
            if type(specificLocation) is tuple:
                add = specificLocation == startEnd
            else:
                add = specificLocation in startEnd
            if add:
                wpsSpecificLocation['both'][startEnd] = response['waypoints']
        elif 'Suez' in response['crossedCanals']:
            dists['suez'][startEnd] = response['distance']
            wps['suez'][startEnd] = response['waypoints']
            if type(specificLocation) is tuple:
                add = specificLocation == startEnd
            else:
                add = specificLocation in startEnd
            if add:
                wpsSpecificLocation['suez'][startEnd] = response['waypoints']
        elif 'Panama' in response['crossedCanals']:
            dists['panama'][startEnd] = response['distance']
            wps['panama'][startEnd] = response['waypoints']
            if type(specificLocation) is tuple:
                add = specificLocation == startEnd
            else:
                add = specificLocation in startEnd
            if add:
                wpsSpecificLocation['panama'][startEnd] = response['waypoints']
        else:
            dists['none'][startEnd] = response['distance']
            wps['none'][startEnd] = response['waypoints']
            if type(specificLocation) is tuple:
                add = specificLocation == startEnd
            else:
                add = specificLocation in startEnd
            if add:
                wpsSpecificLocation['none'][startEnd] = response['waypoints']

wpsSpecificLocation['all'] = {}
for canals in ['both', 'panama', 'suez', 'none']:
    for startEnd, pathWaypoints in wpsSpecificLocation[canals].items():
        start, end = startEnd
        if start == specificLocation:
            key = end + ' x ' + canals
        else:
            key = start + ' x ' + canals

        wpsSpecificLocation['all'][key] = pathWaypoints

wpsDict = wpsSpecificLocation

pp.pprint(list(wpsDict[inclCanals].keys()))
pp.pprint(wpsDict)

nPlots = len(wpsDict[inclCanals])

rows = int(round(math.sqrt(nPlots)))
columns = int(math.ceil(math.sqrt(nPlots)))

for i, (startEnd, pathWaypoints) in enumerate(wpsDict[inclCanals].items()):
    axis = plt.subplot(rows, columns, i+1)
    if type(startEnd) is tuple:
        titleIn = '{} - {}'.format(startEnd[0], startEnd[1])
    else:
        titleIn = startEnd
    plot_routes(pathWaypoints, axis, titleIn)

plt.show()

# Delete speeds
for xCanals, routes in wps.items():
    for route, waypointsList in routes.items():
        for waypoint in waypointsList:
            if 'speed' in waypoint:
                del waypoint['speed']

distDF = pd.DataFrame.from_dict(dists).sort_index()
wpsDF = pd.DataFrame.from_dict(wps).sort_index()

writer = pd.ExcelWriter('distances.xlsx')
distDF.to_excel(writer, sheet_name='distances')
wpsDF.to_excel(writer, sheet_name='waypoints')
writer.save()

print(wpsDF.head())
