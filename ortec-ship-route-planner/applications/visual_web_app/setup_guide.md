# Setup guide: Route Visualization Web Application

## Api setup
1. Install Python 3
2. In a cmd prompt type: `pip install flask`
3. In a cmd prompt type: `pip install flask_cors`

## Api run
1. Start cmd prompt from "visual_web_app/RouteNavigation.API/src"
2. Type `python route-navigation-api.py`
3. The cmd prompt output should indicate: `Running on http://127.0.0.1:5000/`

## UI setup
1. Install [nodejs](https://nodejs.org/en/)
2. Install [Visual Studio Code](https://code.visualstudio.com/)
3. In a cmd prompt type: `npm install -g @angular/cli`
4. In Visual Studio Code, open folder: "visual_web_app/RouteNavigation.UI/route-navigation-ui"
5. Open a new terminal (top bar -> terminal -> new terminal)
6. Type `npm install` (might take a while)

## UI run
1. From Visual Studio Code, type in a terminal `ng serve`
2. The terminal output should indictate: `** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **`
3. In your browser, surf to http://localhost:4200/. The website should be rendered.