import uuid
import pprint
import main

from datetime import datetime
from flask import request, jsonify, Flask
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
# os.chdir('C:/dev/projects/naval_route_planner')

# list of input configurations for requested routes
requested_routes = []


# request a new route
@app.route('/request_route', methods=['POST'])
def request_route():
	input_config = request.get_json()  # get input configuration from the request
	ID = str(uuid.uuid4())  # create random id
	input_config['id'] = ID  # put id on the input configuration
	requested_routes.append(input_config)  # append input configuration to the list of requested routes
	return jsonify({'id': ID})


# trigger route calculation
@app.route('/route', methods=['GET'])
def get_route():
	# retrieve id from the request. If there is no id property on the request, return an error
	if 'id' in request.args:
		ID = request.args['id']
	else:
		return jsonify({'error': 'No id field provided. Please specify an id.'})

	# search the list of requested routes for an input configuration with the correct id
	for r in requested_routes:
		if r['id'] == ID:
			requested_routes.remove(r)  # remove the input configuration r from the list
			pp = pprint.PrettyPrinter()
			pp.pprint(r)

			# Get input
			criteria = r['optimizationCriteria']
			startEnd = ((r['from']['lon'], r['from']['lat']),
						(r['to']['lon'], r['to']['lat']))
			vessel = r['ship']
			fuelPrice = float(r['fuelPrice'])
			ecaFactor = float(r['secaMultiplier'])
			current = r['currents']
			weather = r['weather']

			if 'startDate' in r:
				startDate = datetime.strptime(r['startDate'] + r['startTime'], '%a %b %d %Y%H:%M:%S')
			else:
				startDate = None

			# Initialize RoutePlanner class with general parameters
			planner = main.RoutePlanner(vesselName=vessel,
										ecaFactor=ecaFactor,
										fuelPrice=fuelPrice,
										criteria=criteria,
										seed=1)

			# Find route for given start and end coordinates
			results = planner.compute(startEnd,
									  startDate=startDate,
									  current=current,
									  weather=weather)

			routeCalculateResponse, _ = planner.post_process(results, ID)
			pp.pprint(routeCalculateResponse)

			return jsonify(routeCalculateResponse)

	# if there is no requested route with the correct id, return an error
	return jsonify({'error': 'No requested route found for id: ' + ID})


app.run()
