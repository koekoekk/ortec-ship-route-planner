from data_config import current_data, wind_data
import math
import numpy as np

from dask.cache import Cache


class CurrentOperator:
    def __init__(self, t0, nDays):
        self.t0 = t0.replace(second=0, microsecond=0, minute=0, hour=0)
        self.nDays = nDays
        self.data = np.array(current_data.CurrentDataRetriever(self.t0, nDays).get_data())

        cache = Cache(2e9)  # Leverage two gigabytes of memory
        cache.register()  # Turn cache on globally

    def get_grid_pt_current(self, date_in, lon, lat):
        hourPeriod = 3
        lonIdx = int(round((lon + 179.875) / 0.25))
        latIdx = int(round((lat + 89.875) / 0.25))
        delta = date_in - self.t0
        if delta.days < self.nDays:
            dayIdx = int(delta.seconds / 3600 // hourPeriod)
            vals = self.data[:, dayIdx, latIdx, lonIdx]
            u, v = vals[0], vals[1]

            if math.isnan(u) or math.isnan(v):
                u = v = 0.0
        else:
            u = v = 0.0

        return u, v


def find_nearest_idx(array, value):
    array = np.asarray(array)
    return (np.abs(array - value)).argmin()


class WindOperator:
    def __init__(self, t0, nDays, forecast):
        self.t0 = t0.replace(second=0, microsecond=0, minute=0, hour=0)

        if forecast:
            assert nDays * 8 < 384, 'Estimated travel days exceeds wind forecast period'
        self.data = wind_data.WindDataRetriever(startDate=self.t0, nDays=nDays).get_data(forecast=forecast)

        cache = Cache(2e9)  # Leverage two gigabytes of memory
        cache.register()  # Turn cache on globally

    def get_grid_pt_wind(self, time, lon, lat):
        resolution = 0.5
        hourPeriod = 6
        lon_idx = int(round((lon + 180) / resolution))
        lon_idx = 0 if lon_idx == 720 else lon_idx  # data has no cyclic column; hence, refer long 180 to -180
        lat_idx = int(round((lat + 90) / resolution))
        step_idx = int((time - self.t0).seconds / 3600 // hourPeriod)
        vals = self.data[:, step_idx, lat_idx, lon_idx]
        BN = vals[0]
        TWD = vals[1]

        if math.isnan(BN) or math.isnan(TWD):
            BN, TWD = 0, 0.0

        return BN, TWD
