ORTEC Ship Route Planner v2.0
--------

To use, do::

    >>> from ortec-ship-route-planner import RoutePlanner
    >>> planner = RoutePlanner(**kwargsPlanner)
	>>> result = planner.compute(startEnd, **kwargsCompute)
	
Keyword arguments `RoutePlanner` `kwargsPlanner` are ::
*	`constantSpeedIdx` (default `None`),
*	`vesselName` (default `Fairmaster_2`),
*	`shipLoading` (default `normal`),
*	`ecaFactor` (default `1.5593`),
*	`fuelPrice` (default `300`),
*	`bathymetry` (default `False`)
*	`inputParameters` (default `None`)
*	`tb` (default `None`)
*	`criteria` (default `None`)
*	`seed` (default `None`)

	
Keyword arguments `compute` `kwargsCompute` are ::
*	`recompute` (default `False`),
*	`startDate` (default `None`),
*	`current` (default `False`),
*	`weather` (default `False`),
*	`avoidArctic` (default `True`),
*	`avoidAntarctic` (default `True`)